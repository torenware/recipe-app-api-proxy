server {
    listen ${LISTEN_PORT};

    # We serve static files from nginx, and pass python to
    # uwsgi

    location /static {
        alias /vol/static;
    }

    location / {
        uwsgi_pass ${APP_HOST}:${APP_PORT};
        include /etc/nginx/uwsgi_params;
        # Since we're uploading images:
        client_max_body_size 10M;
    }
}