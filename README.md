# Recipe App API Proxy

Nginx proxy used for the demo RA project.

## Usage

### Environment variables

* `LISTEN_PORT`: Port the proxy will listen on (default: 8000)
* `APP_HOST`: Hostname of the app behind the proxy (default: `app`)
* `APP_PORT`: Port used by the app behind the proxy.


