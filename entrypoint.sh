#! /bin/sh

# Exit with error if anything fails:
set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Run in foreground as recommended for Docker images.
nginx -g 'daemon off;'

